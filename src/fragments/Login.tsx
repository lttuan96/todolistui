import React from "react";
import {
    makeStyles,
    createStyles,
    Theme
} from "@material-ui/core/styles";
import {
    Box,
    TextField,
    InputAdornment,
    Button,
    Checkbox,
    FormControlLabel,
    Typography,
    IconButton,
    CircularProgress
} from "@material-ui/core";
import {
    PersonOutlineOutlined,
    LockOutlined,
    Visibility,
    VisibilityOff
} from "@material-ui/icons";
import axios from "axios";

const useStyles = makeStyles((theme: Theme) => createStyles({
    loginInput: {
        width: "90%",
        margin: "0.5em",
    }
}));

function LoginFragment(props: any) {
    const classes = useStyles();
    const formRef = React.useRef<HTMLFormElement>(null);
    const [isUsernameValid, setIsUsernameValid] = React.useState<boolean>(true);
    const [isPasswordValid, setIsPasswordValid] = React.useState<boolean>(true);
    const [isPasswordVisible, setIsPasswordVisible] = React.useState<boolean>(false);
    const [isUserRemember, setIsUserRemember] = React.useState<boolean>(false);
    const [isLoading, setIsLoading] = React.useState<boolean>(false);
    const [usernameInput, setUsernameInput] = React.useState<string>("");
    const [passwordInput, setPasswordInput] = React.useState<string>("");
    const { onCancel, onSubmit, onFeedback } = props;

    const handleUsernameInputChange = (event: any) => {
        setUsernameInput(event.target.value);
        setIsUsernameValid(ValidateUsername(event.target.value));
    }

    const handlePasswordInputChange = (event: any) => {
        setPasswordInput(event.target.value);
        setIsPasswordValid(ValidatePassword(event.target.value));
    }

    const handleLoginSubmit = () => {
        setIsLoading(true);
        if(formRef.current?.checkValidity()) {
            if (ValidateUsername(usernameInput) && ValidatePassword(passwordInput)) {
                const targetUrl = `${process.env.REACT_APP_PROTOCOL}://${process.env.REACT_APP_URL}:${process.env.REACT_APP_PORT}/login`;
                const bodyData = {
                    "username": usernameInput,
                    "password": passwordInput
                }
    
                axios.post(targetUrl, bodyData)
                    .then(res => {
                        onFeedback({
                            type: "success",
                            message: "Login successfully"
                        })

                        const submitData = {
                            Username: usernameInput,
                            Token: res.data
                        }
                        onSubmit(submitData);
                                                               
                        if (isUserRemember) {
                            localStorage.setItem("loginToken", res.data);
                            localStorage.setItem("loginUsername", usernameInput);
                        }

                        setUsernameInput("");
                    })
                    .catch(err => {
                        onFeedback({
                            type: "error",
                            message: "Incorrect username or password"           
                        })
                        setPasswordInput("");
                    });
            }
        }else {
            formRef.current?.reportValidity();   
        }
               
        setIsLoading(false);
    }

    const handleCancelLogin = (event: React.MouseEvent) => {
        if (event.target === event.currentTarget) {
            onCancel();
            setUsernameInput("");
            setPasswordInput("");
        }   
    }

    return (
        <Box onClick={handleCancelLogin} position="fixed" zIndex={9} width="100%" height="100%" style={{ backgroundColor: "rgba(186, 186, 186, 0.2)" }} alignItems="center" justifyItems="center">
            <Box position="relative" margin="30vh auto" bgcolor="white" width="100%" maxWidth="800px" textAlign="center" borderRadius="10px" style={{ boxShadow: "2px 4px lightgrey" }}>
                <Typography variant="h4">Login</Typography>
                <form ref={formRef}>
                    <TextField
                        required
                        className={classes.loginInput}
                        onChange={handleUsernameInputChange}
                        variant="outlined"
                        placeholder="Username"
                        helperText="Allows number and the following characters: -._@+"
                        error={!isUsernameValid}
                        value={usernameInput}
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <PersonOutlineOutlined />
                                </InputAdornment>
                            )
                        }}
                    />
                    <TextField
                        required
                        className={classes.loginInput}
                        onChange={handlePasswordInputChange}
                        variant="outlined"
                        placeholder="Password"
                        helperText="Requires atleast six characters long, an uppercase, a lowercase, a digit, and a non-alphanumeric character."
                        error={!isPasswordValid}
                        value={passwordInput}
                        type={isPasswordVisible ? "text" : "password"}
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <LockOutlined />
                                </InputAdornment>
                            ),
                            endAdornment: (
                                <InputAdornment position="end">
                                    {isPasswordVisible ? <IconButton onClick={() => setIsPasswordVisible(false)}><VisibilityOff /></IconButton> : <IconButton onClick={() => setIsPasswordVisible(true)}><Visibility /></IconButton>}
                                </InputAdornment>
                            )
                        }}
                    />

                    <Button onClick={handleLoginSubmit} style={{ border: "2px solid gray" }} variant="outlined">{isLoading ? <CircularProgress style={{margin: "0 0.74em"}} size="1.75em" /> : "Login"}</Button>
                    <br ></br>
                    <FormControlLabel
                        onClick={() => setIsUserRemember(!isUserRemember)}
                        checked={isUserRemember}
                        control={
                            <Checkbox
                                color="primary"
                                name="remember-me"
                            />
                        }
                        label="Remember me?"
                    />
                </form>

            </Box>
        </Box>
    );
}

function ValidatePassword(content: string): boolean {
    return content.length >= 6 && /[A-Z]/.test(content) && /[a-z]/.test(content) && /[\d]/.test(content) && /[!@#&()–[{}\]:;',?/*~$^+=<>]/.test(content);
}

function ValidateUsername(content: string): boolean {
    return !/[^A-z0-9-._@+]/.test(content) && !/[\^[\]\\]/.test(content);
}

export default LoginFragment;