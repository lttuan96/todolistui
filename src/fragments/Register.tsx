import React from "react";
import {
    makeStyles,
    createStyles,
    Theme
} from "@material-ui/core/styles";
import {
    Box,
    TextField,
    InputAdornment,
    Button,
    Typography,
    IconButton,
    CircularProgress
} from "@material-ui/core";
import {
    PersonOutlineOutlined,
    LockOutlined,
    Visibility,
    VisibilityOff,
    MailOutline
} from "@material-ui/icons";

const useStyles = makeStyles((theme: Theme) => createStyles({
    loginInput: {
        width: "90%",
        margin: "0.5em",
    }
}));

function RegisterFragment(props: any) {
    const classes = useStyles();
    const formRef = React.useRef<HTMLFormElement>(null);
    const [isUsernameValid, setIsUsernameValid] = React.useState<boolean>(true);
    const [isPasswordValid, setIsPasswordValid] = React.useState<boolean>(true);
    const [isPasswordVisible, setIsPasswordVisible] = React.useState<boolean>(false);
    const [isLoading, setIsLoading] = React.useState<boolean>(false);
    const [usernameInput, setUsernameInput] = React.useState<string>("");
    const [passwordInput, setPasswordInput] = React.useState<string>("");
    const [emailInput, setEmailInput] = React.useState<string>("");
    const { onCancel, onFeedback } = props;

    const handleUsernameInputChange = (event: any) => {
        setUsernameInput(event.target.value);
        setIsUsernameValid(ValidateUsername(event.target.value));
    }

    const handlePasswordInputChange = (event: any) => {
        setPasswordInput(event.target.value);
        setIsPasswordValid(ValidatePassword(event.target.value));
    }

    const handleRegisterSubmit = () => {
        setIsLoading(true);

        setTimeout(() => {
            if(formRef.current?.checkValidity()) {
                if (ValidateUsername(usernameInput) && ValidatePassword(passwordInput) && emailInput.length > 6) {
                    onFeedback({
                        "type": "success",
                        "message": "Registration feature is currently closed"
                    })
                }
            }else {
                formRef.current?.reportValidity();
            }

            setIsLoading(false);
        }, 2000)
    }

    const handleCancelRegister = (event: React.MouseEvent) => {
        if (event.target === event.currentTarget) {
            onCancel();
            setEmailInput("");
            setUsernameInput("");
            setPasswordInput("");
        }    
    }

    return (
        <Box onClick={handleCancelRegister} position="fixed" zIndex={10} width="100%" height="100%" style={{ backgroundColor: "rgba(186, 186, 186, 0.2)" }} alignItems="center" justifyItems="center">
            <Box position="relative" margin="30vh auto" bgcolor="white" width="100%" maxWidth="800px" textAlign="center" borderRadius="10px" style={{ boxShadow: "2px 4px lightgrey" }}>
                <Typography variant="h4">Register</Typography>
                <form ref={formRef}>
                    <TextField
                        required
                        className={classes.loginInput}
                        onChange={(event: any) => setEmailInput(event.target.value)}
                        variant="outlined"
                        placeholder="Email"
                        type="email"
                        helperText="We'll never share your email."
                        value={emailInput}
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <MailOutline />
                                </InputAdornment>
                            )
                        }}
                    />
                    <TextField
                        required
                        className={classes.loginInput}
                        onChange={handleUsernameInputChange}
                        variant="outlined"
                        placeholder="Username"
                        helperText="Allows number and the following characters: -._@+"
                        error={!isUsernameValid}
                        value={usernameInput}
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <PersonOutlineOutlined />
                                </InputAdornment>
                            )
                        }}
                    />
                    <TextField
                        required
                        className={classes.loginInput}
                        onChange={handlePasswordInputChange}
                        variant="outlined"
                        placeholder="Password"
                        helperText="Requires atleast six characters long, an uppercase, a lowercase, a digit, and a non-alphanumeric character."
                        error={!isPasswordValid}
                        value={passwordInput}
                        type={isPasswordVisible ? "text" : "password"}
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <LockOutlined />
                                </InputAdornment>
                            ),
                            endAdornment: (
                                <InputAdornment position="end">
                                    {isPasswordVisible ? <IconButton onClick={() => setIsPasswordVisible(false)}><VisibilityOff /></IconButton> : <IconButton onClick={() => setIsPasswordVisible(true)}><Visibility /></IconButton>}
                                </InputAdornment>
                            )
                        }}
                    />

                    <Button onClick={handleRegisterSubmit} style={{ border: "2px solid gray", marginBottom: "0.5em" }} variant="outlined">{isLoading ? <CircularProgress style={{margin: "0 1.75em"}} size="1.75em"/> : "Register"}</Button>
                </form>
            </Box>
        </Box>
    );
}

function ValidatePassword(content: string): boolean {
    return content.length >= 6 && /[A-Z]/.test(content) && /[a-z]/.test(content) && /[\d]/.test(content) && /[!@#&()–[{}\]:;',?/*~$^+=<>]/.test(content);
}

function ValidateUsername(content: string): boolean {
    return !/[^A-z0-9-._@+]/.test(content) && !/[\^[\]\\]/.test(content);
}

export default RegisterFragment;