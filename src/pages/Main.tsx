import React from "react";
import {
    makeStyles,
    createStyles,
    Theme
} from "@material-ui/core/styles";
import {
    Box,
    Typography,
    Button,
    AppBar,
    Toolbar,
    Container,
    TextField,
    Checkbox,
    IconButton,
    Fade,
    Slide
} from "@material-ui/core";
import {
    Delete
} from "@material-ui/icons";
import {
    green,
    red,
    grey
} from "@material-ui/core/colors";

import axios, {AxiosError, AxiosResponse} from "axios";

import LoginFragment from "../fragments/Login";
import RegisterFragment from "../fragments/Register";

const useStyles = makeStyles((theme: Theme) => createStyles({
    appbarTitle: {
        flexGrow: 1
    },
    appbarButton: {
        marginLeft: "0.75em"
    }
}));

interface TodoItem {
    id: string,
    content: string,
    isDone: boolean
}

interface UserInfo {
    name: string
}

interface FeedbackInfo {
    type: string,
    message: string
}

const FeedbackColorMap = {
    "success": green['A200'],
    "error": red["A200"]
}

function MainPage() {
    const classes = useStyles();

    const [todoList, setTodoList] = React.useState<Array<TodoItem>>([]);
    const [userInfo, setUserInfo] = React.useState<UserInfo>();
    const [feedbackInfo, setFeedbackInfo] = React.useState<FeedbackInfo>();
    const [isLoggedIn, setIsLoggedIn] = React.useState<boolean>(false);
    const [toggleLogin, setToggleLogin] = React.useState<boolean>(false);
    const [toggleRegister, setToggleRegister] = React.useState<boolean>(false);
    const [isFeedbackOn, setIsFeedbackOn] = React.useState<boolean>(false);
    const [inputValue, setInputValue] = React.useState<string>("");
    const [cacheToken, setCacheToken] = React.useState<string>("");
    const [currentItem, setCurrentItem] = React.useState<number>(-1);

    React.useEffect(() => {
        const loginToken = localStorage?.getItem("loginToken");
        const userName = localStorage?.getItem("loginUsername");
        if (loginToken && userName) {
            const targetUrl = `${process.env.REACT_APP_PROTOCOL}://${process.env.REACT_APP_URL}:${process.env.REACT_APP_PORT}/`;
            const headerSettings = {
                "Authorization": `Bearer ${loginToken}`
            }

            axios.get(targetUrl, { headers: headerSettings })
                .then((res: AxiosResponse) => {
                    setTodoList(res.data);
                    setIsLoggedIn(true);
                    setCacheToken(loginToken);
                    setUserInfo({
                        name: userName
                    })
                })
                .catch((err: AxiosError) => {
                    handleFeedback({
                        type: "error",
                        message: err.message
                    })
                    localStorage.removeItem("loginToken")
                    window.location.reload();
                })
        } else {
            setTodoList([
                {
                    id: "",
                    content: "Deploy Production",
                    isDone: true
                },
                {
                    id: "",
                    content: "Coverage Test",
                    isDone: false
                }
            ])
        }

    }, [])

    const handleLogOut = () => {
        handleFeedback({
            type: "error",
            message: "Logging out..."
        })

        localStorage.removeItem("loginToken");
        localStorage.removeItem("loginUsername");

        setCacheToken("");
        setTodoList([
            {
                id: "",
                content: "Deploy Production",
                isDone: true
            },
            {
                id: "",
                content: "Coverage Test",
                isDone: false
            }
        ])
        setIsLoggedIn(false);
    }

    const handleItemCheck = (index: number) => {
        if (isLoggedIn && cacheToken) {
            todoList[index].isDone = !todoList[index].isDone;

            const targetUrl = `${process.env.REACT_APP_PROTOCOL}://${process.env.REACT_APP_URL}:${process.env.REACT_APP_PORT}/`;
            const headerSettings = {
                "Authorization": `Bearer ${cacheToken}`
            }
            const bodyData = {
                ...todoList[index]
            }

            axios.put(targetUrl, bodyData, { headers: headerSettings })
                .then((res: AxiosResponse) => {
                    setTodoList([...todoList]);
                })
                .catch((err: AxiosError) => {
                    handleFeedback({
                        type: "error",
                        message: err.message
                    })
                });
        } else {
            todoList[index].isDone = !todoList[index].isDone;
            setTodoList([...todoList]);
        }
    }

    const handleItemDelete = () => {
        if (isLoggedIn && cacheToken) {
            const targetUrl = `${process.env.REACT_APP_PROTOCOL}://${process.env.REACT_APP_URL}:${process.env.REACT_APP_PORT}/${todoList[currentItem].id}`;
            const headerSettings = {
                "Authorization": `Bearer ${cacheToken}`
            }

            axios.delete(targetUrl, { headers: headerSettings })
                .then((res: AxiosResponse) => {
                    todoList.splice(currentItem, 1);
                    setTodoList([...todoList]);
                })
                .catch((err: AxiosError) => {
                    handleFeedback({
                        type: "error",
                        message: err.message
                    })
                })
        } else {
            todoList.splice(currentItem, 1);
            setTodoList([...todoList]);
        }
    }

    const handleInputSubmit = (event: React.KeyboardEvent) => {
        if (event.key !== "Enter" || inputValue === "") return;

        if(todoList.length <= 20) {
            if (isLoggedIn && cacheToken) {
                const targetUrl = `${process.env.REACT_APP_PROTOCOL}://${process.env.REACT_APP_URL}:${process.env.REACT_APP_PORT}/`;
                const headerSettings = {
                    "Authorization": `Bearer ${cacheToken}`
                }
                const bodyData = {
                    "content": inputValue
                }
    
                axios.post(targetUrl, bodyData, { headers: headerSettings })
                    .then((res: AxiosResponse) => {
                        setTodoList([{
                            id: res.data,
                            content: inputValue,
                            isDone: false
                        }, ...todoList
                        ])
                    })
                    .catch((err: AxiosError) => {
                        handleFeedback({
                            type: "error",
                            message: err.message
                        })
                    })
            } else {
                setTodoList([{
                    id: "",
                    content: inputValue,
                    isDone: false
                }, ...todoList
                ]);
            }
        } else {
            handleFeedback({
                type: "error",
                message: "TodoList can only carries upto 20 rows."
            })
        }

        
        setInputValue("");
    }

    const handleUserInfo = (data: any) => {
        const { Username, Token } = data;

        const targetUrl = `${process.env.REACT_APP_PROTOCOL}://${process.env.REACT_APP_URL}:${process.env.REACT_APP_PORT}/`;
        const headerSettings = {
            "Authorization": `Bearer ${Token}`
        }

        axios.get(targetUrl, { headers: headerSettings })
            .then((res: AxiosResponse) => {
                setTodoList(res.data);
                setCacheToken(Token);
                setUserInfo({
                    ...userInfo,
                    name: Username
                });
                setIsLoggedIn(true);
            })
            .catch((err: AxiosError) => {
                handleFeedback({
                    type: "error",
                    message: err.message
                })
                window.location.reload();
            });

        if (toggleLogin) setToggleLogin(false);
        if (toggleRegister) setToggleRegister(false);
    }

    const handleFeedback = (data: FeedbackInfo) => {
        setFeedbackInfo({...data});
        setIsFeedbackOn(true);
        
        setTimeout(() => {
            setIsFeedbackOn(false);
            setFeedbackInfo(undefined);
        }, 2000);
    }

    const UserAccessComponent = () => (
        <>
            <Button onClick={() => setToggleRegister(true)} className={classes.appbarButton} color="inherit">Register</Button>
            <Button onClick={() => setToggleLogin(true)} className={classes.appbarButton} color="inherit" variant="outlined">Login</Button>
        </>
    );

    const UserLoggedInComponent = () => (
        <>
            <Typography>Hello {userInfo?.name}</Typography>
            <Button className={classes.appbarButton} onClick={handleLogOut} color="inherit" variant="outlined" >Logout</Button>
        </>
    );

    return (
        <>
            <Slide in={isFeedbackOn} direction="down" mountOnEnter unmountOnExit>
                <Box paddingY="0.25em" bgcolor= {feedbackInfo ? FeedbackColorMap[feedbackInfo.type as keyof typeof FeedbackColorMap] : grey['50']} color="#242424" textAlign="center">{feedbackInfo?.message}</Box>
            </Slide>
            <Fade in={toggleLogin}>
                <div>
                    <LoginFragment onSubmit={handleUserInfo} onCancel={() => setToggleLogin(false)} onFeedback={handleFeedback}/>
                </div>
            </Fade>
            <Fade in={toggleRegister}>
                <div>
                    <RegisterFragment onCancel={() => setToggleRegister(false)} onFeedback={handleFeedback}/>
                </div>
            </Fade>
            <AppBar position="relative" style={{zIndex: 5}}>
                <Toolbar>
                    <Typography className={classes.appbarTitle} variant="h4">TodoList</Typography>
                    {isLoggedIn ? <UserLoggedInComponent /> : <UserAccessComponent />}
                </Toolbar>
            </AppBar>
            <Container style={{ marginTop: "1em" }} maxWidth="md">
                <TextField value={inputValue} onChange={(event: any) => setInputValue(event.target.value)} onKeyPress={handleInputSubmit} fullWidth variant="outlined" placeholder="Write down your plan here..." />
                {todoList.map((item: TodoItem, index: number) => (
                    <Box onMouseEnter={() => setCurrentItem(index)} onMouseLeave={() => setCurrentItem(-1)} display="flex" width="100%" alignItems="center" key={index}>
                        <Checkbox onChange={() => handleItemCheck(index)} checked={item.isDone} color="primary" />
                        <Typography style={{ flexGrow: 1, margin: "0.8em 0.5em", textDecoration: item.isDone ? "line-through" : "none" }}>{item.content}</Typography>
                        {index === currentItem ? <IconButton onClick={handleItemDelete} aria-label="delete-item"><Delete /></IconButton> : ""}
                    </Box>
                ))}
            </Container>
        </>
    );
}

export default MainPage;